import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { OurteamComponent } from './ourteam/ourteam.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { LifeComponent } from './life/life.component';
import { CareersComponent } from './careers/careers.component';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { DisclaimerPrivacyPolicyComponent } from './disclaimer-privacy-policy/disclaimer-privacy-policy.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OurteamComponent,
    ContactComponent,
    AboutComponent,
    PortfolioComponent,
    LifeComponent,
    CareersComponent,
    DisclaimerPrivacyPolicyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [{provide: LocationStrategy, useClass: HashLocationStrategy},],
  bootstrap: [AppComponent]
})
export class AppModule { }
