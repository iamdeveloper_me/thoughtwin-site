import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  loaderState:boolean = true;
  constructor() {
    $.getScript('./assets/js/script.js');

   }
  show:boolean = false;
  burgerState:boolean = false;
  ngOnInit() {
    this.loaderState = false;
  }
  burger(){
    this.show = !this.show;
    if(this.show == true){
      this.burgerState = true; 
    }else{
      this.burgerState = false;
    }
  }
}
