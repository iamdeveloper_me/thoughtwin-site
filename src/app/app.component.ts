import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'thoughtwin';

constructor(){
  $.getScript('./assets/js/script.js');

}
  ngOnInit(){
//      $(document).ready(function(){
//         $('[data-toggle="tooltip"]').tooltip(); 
    
//      });
    
// // vars
// 'use strict'
// var	testim = document.getElementById("testim"),
// 		testimDots = Array.prototype.slice.call($("#testim-dots").children),
//     testimContent = Array.prototype.slice.call($("#testim-content").children),
//     testimLeftArrow = $("#left-arrow"),
//     testimRightArrow = $("#right-arrow"),
//     testimSpeed = 4500,
//     currentSlide = 0,
//     currentActive = 0,
//     testimTimer,
// 		touchStartPos,
// 		touchEndPos,
// 		touchPosDiff,
// 		ignoreTouch = 30;
// ;

// window.onload = function() {

//     // Testim Script
//     function playSlide(slide) {
//         for (var k = 0; k < testimDots.length; k++) {
//             testimContent[k].classList.remove("active");
//             testimContent[k].classList.remove("inactive");
//             testimDots[k].classList.remove("active");
//         }

//         if (slide < 0) {
//             slide = currentSlide = testimContent.length-1;
//         }

//         if (slide > testimContent.length - 1) {
//             slide = currentSlide = 0;
//         }

//         if (currentActive != currentSlide) {
//             testimContent[currentActive].classList.add("inactive");            
//         }
//         testimContent[slide].classList.add("active");
//         testimDots[slide].classList.add("active");

//         currentActive = currentSlide;
    
//         clearTimeout(testimTimer);
//         testimTimer = setTimeout(function() {
//             playSlide(currentSlide += 1);
//         }, testimSpeed)
//     }

//     testimLeftArrow.addEventListener("click", function() {
//         playSlide(currentSlide -= 1);
//     })

//     testimRightArrow.addEventListener("click", function() {
//         playSlide(currentSlide += 1);
//     })    

//     for (var l = 0; l < testimDots.length; l++) {
//         testimDots[l].addEventListener("click", function() {
//             playSlide(currentSlide = testimDots.indexOf(this));
//         })
//     }

//     playSlide(currentSlide);

//     // keyboard shortcuts
//     document.addEventListener("keyup", function(e) {
//         switch (e.keyCode) {
//             case 37:
//                 testimLeftArrow.click();
//                 break;
                
//             case 39:
//                 testimRightArrow.click();
//                 break;

//             case 39:
//                 testimRightArrow.click();
//                 break;

//             default:
//                 break;
//         }
//     })
		
// 		testim.addEventListener("touchstart", function(e) {
// 				touchStartPos = e.changedTouches[0].clientX;
// 		})
	
// 		testim.addEventListener("touchend", function(e) {
// 				touchEndPos = e.changedTouches[0].clientX;
			
// 				touchPosDiff = touchStartPos - touchEndPos;
			
// 				console.log(touchPosDiff);
// 				console.log(touchStartPos);	
// 				console.log(touchEndPos);	

			
// 				if (touchPosDiff > 0 + ignoreTouch) {
// 						testimLeftArrow.click();
// 				} else if (touchPosDiff < 0 - ignoreTouch) {
// 						testimRightArrow.click();
// 				} else {
// 					return;
// 				}
			
// 		})
// };
//  (function() {
  
//   var Menu = (function() {
//     var burger = document.querySelector('.burger');
//     var menu = document.querySelector('.menu');
//     var menuList = document.querySelector('.menu__list');
//     var brand = document.querySelector('.menu__brand');
//     var menuItems = document.querySelectorAll('.menu__item');
//     debugger
//     var active = false;
    
//     var toggleMenu = function() {
//       if (!active) {
//         menu.classList.add('menu--active');
//         menuList.classList.add('menu__list--active');
//         brand.classList.add('menu__brand--active');
//         burger.classList.add('burger--close');
//         for (var i = 0, ii = menuItems.length; i < ii; i++) {
//           menuItems[i].classList.add('menu__item--active');
//         }
        
//         active = true;
//       } else {
//         menu.classList.remove('menu--active');
//         menuList.classList.remove('menu__list--active');
//         brand.classList.remove('menu__brand--active');
//         burger.classList.remove('burger--close');
//         for (var i = 0, ii = menuItems.length; i < ii; i++) {
//           menuItems[i].classList.remove('menu__item--active');
//         }
        
//         active = false;
//       }
//     };
    
//     var bindActions = function() {
//       burger.addEventListener('click', toggleMenu, false);
//     };
    
//     var init = function() {
//       bindActions();
//     };
    
//     return {
//       init: init
//     };
    
//   }());
  
//   Menu.init();
  
// }());

  }
}
