import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisclaimerPrivacyPolicyComponent } from './disclaimer-privacy-policy.component';

describe('DisclaimerPrivacyPolicyComponent', () => {
  let component: DisclaimerPrivacyPolicyComponent;
  let fixture: ComponentFixture<DisclaimerPrivacyPolicyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisclaimerPrivacyPolicyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisclaimerPrivacyPolicyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
