import { CareersComponent } from './careers/careers.component';
import { PortfolioComponent } from './portfolio/portfolio.component';
import { AboutComponent } from './about/about.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OurteamComponent } from './ourteam/ourteam.component';
import { ContactComponent } from './contact/contact.component';
import { LifeComponent } from './life/life.component';
import { DisclaimerPrivacyPolicyComponent } from './disclaimer-privacy-policy/disclaimer-privacy-policy.component';

const routes: Routes = [
  { path: '', 
    component: HomeComponent 
  },
  { path: 'ourteam', 
    component: OurteamComponent 
  },
  { path: 'contact', 
  component: ContactComponent 
  },
  { path: 'about', 
  component: AboutComponent 
  },
  { path: 'portfolio', 
  component: PortfolioComponent 
  },
  { path: 'life', 
  component: LifeComponent 
  },
  { path: 'careers', 
  component: CareersComponent 
  },
  { path: 'disclaimer_privacy_policy', 
  component: DisclaimerPrivacyPolicyComponent 
  },
  

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
