import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";



@Component({
  selector: 'app-portfolio',
  templateUrl: './portfolio.component.html',
  styleUrls: ['./portfolio.component.css']
})
export class PortfolioComponent implements OnInit {

  constructor() { 
    $.getScript('./assets/js/script.js');

  }
  show:boolean = false;
  burgerState:boolean = false;
  ngOnInit() {
  }
  burger(){
    this.show = !this.show;
    if(this.show == true){
      this.burgerState = true; 
    }else{
      this.burgerState = false;
    }
  }

}
