import { Component, OnInit } from '@angular/core';
import * as $ from "jquery";


@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor() {
  $.getScript('./assets/js/script.js');
  $.getScript('./assets/js/contact.js');


   }
  show:boolean = false;
  burgerState:boolean = false;
  ngOnInit() {
  }
  burger(){
    this.show = !this.show;
    if(this.show == true){
      this.burgerState = true; 
    }else{
      this.burgerState = false;
    }
  }

}
